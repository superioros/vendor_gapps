# Automatically generated file. DO NOT MODIFY
#
# This file is generated by device/gapps/arm64/setup-makefiles.sh

PRODUCT_SOONG_NAMESPACES += \
    vendor/gapps/arm64

PRODUCT_PACKAGES += \
    GmsCore \
    Phonesky

ifeq ($(TARGET_IS_GROUPER),)

PRODUCT_PACKAGES += \
    MarkupGoogle \
    SpeechServicesByGoogle \
    talkback \
    Velvet \
    SetupWizard
endif

ifneq ($(filter %tangorpro,$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += VelvetTitan
endif

$(call inherit-product, vendor/gapps/common/common-vendor.mk)
